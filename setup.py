from setuptools import find_packages
from setuptools import setup

REQUIRED_PACKAGES = ['matplotlib', 'numpy', 'tensorflow', 'biosppy', 'wfdb', ]

setup(name='ecg_dl',
      version='0.1',
      install_requires=REQUIRED_PACKAGES,
      description='Deep learning methods for ECG classifications',
      url='http://github.com/tomergolany/ecg_dl',
      author='Tomer Golany',
      author_email='tomer.golany@gmail.com',
      license='Technion',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False)
