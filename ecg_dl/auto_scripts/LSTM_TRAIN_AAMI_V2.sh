#!/bin/bash

echo "                                     " "#########" Auto script to Train LSTM network with AAMI standard"#########"
echo
beat_types=(N S V F Q)

for b in ${beat_types[*]}; do

	echo "#########" Trainning LSTM to classifiy $b beats "#####################"
	echo
	sample_number_from_gan=(500 800 1000 1500 3000 5000 7000 10000 15000)
	echo "######" Train with additional 0 samples from GAN: "################"
	echo

	python3 main_model.py --tensor_board_dir logs/aami/conditional_gan/${b}_lstm --saved_models_dir saved_models/aami/conditional_gan/${b}_lstm --beat_to_classify ${b} --mode TRAIN_${b} \
	--database_type AAMI --classifier_architecture LSTM --path_to_save_roc_curve results/aami/conditional_gan/${b}/lstm_0_roc.png --path_to_save_prec_rec results/aami/conditional_gan/${b}/lstm_0_prec_rec.png > ${b}_0.log

	for s in ${sample_number_from_gan[*]}; do

		echo "######" Train with additional $s samples from GAN: "################"
		echo
		
		python3 main_model.py --tensor_board_dir logs/aami/conditional_gan/${b}_lstm_${s}_with_gan --saved_models_dir saved_models/aami/conditional_gan/${b}_lstm_${s}_with_gan \
		--beat_to_classify $b --mode TRAIN_${b} --num_of_examples_to_add_from_gan $s --use_gan --gan_type DC_CONDITIONAL --gan_meta_file saved_models/conditional_gan/iter_1900 \
		--database_type AAMI --classifier_architecture LSTM --path_to_save_roc_curve results/aami/conditional_gan/${b}/lstm_${s}_samples_roc.png \
		--path_to_save_prec_rec results/aami/conditional_gan/${b}/lstm_${s}_samples_prec_rec.png > ${b}_${s}.log

		echo "##################################################################"
		echo
	done

done