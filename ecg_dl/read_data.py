import os
from tensorflow.python.lib.io import file_io
import matplotlib
# matplotlib.use('agg')
import matplotlib.pyplot as plt
import decimal
from biosppy.signals import ecg
import numpy as np
import wfdb

DATA_DIR = os.path.join("Data", "text_files",)


def create_beat_samples_aami(positive_class=None):
    """
    Save samples from Train test and val into .png files
    :param positive_class:
    :return: train, validation and test sets according to aami.
    """

    # 1. Retrieve only the number of each patient and devide the patients to the 3 sets. Each patient has the same
    # number of beats.
    train_set, validation_set, test_set = divide_data_to_train_test_according_to_aami()

    print("************************Statistics*************************************")
    print("Size of Train set: %d, Size of validation set: %d Size of Test set: %d"
          % (len(train_set), len(validation_set), len(test_set)))
    # Statistics of each set:
    print("*******************Train set statistics:*******************************")
    get_statistics_of_beat_types(train_set)

    print("******************Validation set statistics:***************************")
    get_statistics_of_beat_types(validation_set)

    print("*******************Test set statistics*********************************")
    get_statistics_of_beat_types(test_set)
    print("***********************************************************************")

    if positive_class is None:
        validation_beats, validation_tags = merge_data_of_patients(validation_set, binary_pred=False, aami_standarts=True)

        train_beats, train_tags = merge_data_of_patients(train_set, binary_pred=False, aami_standarts=True)

        test_beats, test_tags = merge_data_of_patients(test_set, binary_pred=False, aami_standarts=True)
    else:
        validation_beats, validation_tags = merge_data_of_patients(validation_set, binary_pred=True,
                                                                   postivie_class=positive_class, aami_standarts=True)

        train_beats, train_tags = merge_data_of_patients(train_set, binary_pred=True,
                                                                   postivie_class=positive_class, aami_standarts=True)

        test_beats, test_tags = merge_data_of_patients(test_set, binary_pred=True,
                                                                   postivie_class=positive_class, aami_standarts=True)

    print("************************More Statistics************************************")
    print("*******************Train set statistics:*******************")
    print("Number of N beats(== 0)", list(train_tags).count(0))
    print("Number of S beats(== 1)", list(train_tags).count(1))
    print("Number of V beats(== 2)", list(train_tags).count(2))
    print("Number of F beats(== 3)", list(train_tags).count(3))
    print("Number of Other beats(== 4)", list(train_tags).count(4))

    print("*******************Validation set statistics:*******************")
    print("Number of N beats(== 0)", list(validation_tags).count(0))
    print("Number of S beats(== 1)", list(validation_tags).count(1))
    print("Number of V beats(== 2)", list(validation_tags).count(2))
    print("Number of F beats(== 3)", list(validation_tags).count(3))
    print("Number of Other beats(== 4)", list(validation_tags).count(4))

    print("*******************Test set statistics:*******************")
    print("Number of N beats(== 0)", list(test_tags).count(0))
    print("Number of A beats(== 1)", list(test_tags).count(1))
    print("Number of V beats(== 2)", list(test_tags).count(2))
    print("Number of R beats(== 3)", list(test_tags).count(3))
    print("Number of Other beats(== 4)", list(test_tags).count(4))
    print("*************************************************************************")

    # Save plots of signals of each type:
    inds_of_N_in_train = [i for i, e in enumerate(list(train_tags)) if e == 0]
    inds_of_S_in_train = [i for i, e in enumerate(list(train_tags)) if e == 1]
    inds_of_V_in_train = [i for i, e in enumerate(list(train_tags)) if e == 2]
    inds_of_F_in_train = [i for i, e in enumerate(list(train_tags)) if e == 3]
    inds_of_Q_in_train = [i for i, e in enumerate(list(train_tags)) if e == 4]

    inds_of_N_in_val = [i for i, e in enumerate(list(validation_tags)) if e == 0]
    inds_of_S_in_val = [i for i, e in enumerate(list(validation_tags)) if e == 1]
    inds_of_V_in_val = [i for i, e in enumerate(list(validation_tags)) if e == 2]
    inds_of_F_in_val = [i for i, e in enumerate(list(validation_tags)) if e == 3]
    inds_of_Q_in_val = [i for i, e in enumerate(list(validation_tags)) if e == 4]

    inds_of_N_in_test = [i for i, e in enumerate(list(test_tags)) if e == 0]
    inds_of_S_in_test = [i for i, e in enumerate(list(test_tags)) if e == 1]
    inds_of_V_in_test = [i for i, e in enumerate(list(test_tags)) if e == 2]
    inds_of_F_in_test = [i for i, e in enumerate(list(test_tags)) if e == 3]
    inds_of_Q_in_test = [i for i, e in enumerate(list(test_tags)) if e == 4]

    for j in inds_of_N_in_train[:10]:
        beat = train_beats[j]
        plt.figure()
        plt.plot(beat)
        plt.savefig(os.path.join('samples', 'aami', 'beat_types', 'N', 'train', str(j) + "_N_beat.png"))

    for j in inds_of_S_in_train[:10]:
        beat = train_beats[j]
        plt.figure()
        plt.plot(beat)
        plt.savefig(os.path.join('samples', 'aami', 'beat_types', 'S', 'train', str(j) + "_S_beat.png"))

    for j in inds_of_V_in_train[:10]:
        beat = train_beats[j]
        plt.figure()
        plt.plot(beat)
        plt.savefig(os.path.join('samples', 'aami', 'beat_types', 'V', 'train', str(j) + "_V_beat.png"))

    for j in inds_of_F_in_train[:10]:
        beat = train_beats[j]
        plt.figure()
        plt.plot(beat)
        plt.savefig(os.path.join('samples', 'aami', 'beat_types', 'F', 'train', str(j) + "_F_beat.png"))

    for j in inds_of_Q_in_train[:10]:
        beat = train_beats[j]
        plt.figure()
        plt.plot(beat)
        plt.savefig(os.path.join('samples', 'aami', 'beat_types', 'Q', 'train', str(j) + "_Q_beat.png"))

    for j in inds_of_N_in_val[:10]:
        beat = validation_beats[j]
        plt.figure()
        plt.plot(beat)
        plt.savefig(os.path.join('samples', 'aami', 'beat_types', 'N', 'val', str(j) + "_N_beat.png"))

    for j in inds_of_S_in_val[:10]:
        beat = validation_beats[j]
        plt.figure()
        plt.plot(beat)
        plt.savefig(os.path.join('samples', 'aami', 'beat_types', 'S', 'val', str(j) + "_S_beat.png"))

    for j in inds_of_V_in_val[:10]:
        beat = validation_beats[j]
        plt.figure()
        plt.plot(beat)
        plt.savefig(os.path.join('samples', 'aami', 'beat_types', 'V', 'val', str(j) + "_V_beat.png"))

    for j in inds_of_F_in_val[:10]:
        beat = validation_beats[j]
        plt.figure()
        plt.plot(beat)
        plt.savefig(os.path.join('samples', 'aami', 'beat_types', 'F', 'val', str(j) + "_F_beat.png"))

    for j in inds_of_Q_in_val[:10]:
        beat = validation_beats[j]
        plt.figure()
        plt.plot(beat)
        plt.savefig(os.path.join('samples', 'aami', 'beat_types', 'Q', 'val', str(j) + "_Q_beat.png"))

    for j in inds_of_N_in_test[:10]:
        beat = test_beats[j]
        plt.figure()
        plt.plot(beat)
        plt.savefig(os.path.join('samples', 'aami', 'beat_types', 'N', 'test', str(j) + "_N_beat.png"))

    for j in inds_of_S_in_test[:10]:
        beat = test_beats[j]
        plt.figure()
        plt.plot(beat)
        plt.savefig(os.path.join('samples', 'aami', 'beat_types', 'S', 'test', str(j) + "_S_beat.png"))

    for j in inds_of_V_in_test[:10]:
        beat = test_beats[j]
        plt.figure()
        plt.plot(beat)
        plt.savefig(os.path.join('samples', 'aami', 'beat_types', 'V', 'test', str(j) + "_V_beat.png"))

    for j in inds_of_F_in_test[:10]:
        beat = test_beats[j]
        plt.figure()
        plt.plot(beat)
        plt.savefig(os.path.join('samples', 'aami', 'beat_types', 'F', 'test', str(j) + "_F_beat.png"))

    for j in inds_of_Q_in_test[:10]:
        beat = test_beats[j]
        plt.figure()
        plt.plot(beat)
        plt.savefig(os.path.join('samples', 'aami', 'beat_types', 'Q', 'test', str(j) + "_Q_beat.png"))

    print("Done Sampling")


def merge_data_of_patients(patient_numbers, binary_pred=True, postivie_class='N', aami_standarts=False):
    """

    :param patient_numbers:
    :param binary_pred:
    :param aami_standarts
    :return:
    """
    first_iter = True
    merged_beats = np.array([])
    merged_tags = np.array([])
    for p in patient_numbers:
        # print("Merging patient %s" % p)
        # In y_tags : 1 == Noraml, 0 == Unromal.
        x_ecg_beats, y_tags = create_dataset_for_patient(p, binary_pred=binary_pred, postivie_class=postivie_class,
                                                         aami_standarts=aami_standarts)
        if first_iter:
            first_iter = False
            merged_beats = x_ecg_beats
            merged_tags = y_tags
        else:
            merged_beats = np.concatenate((merged_beats, x_ecg_beats), axis=0)
            merged_tags = np.concatenate((merged_tags, y_tags), axis=0)

    return merged_beats, merged_tags


def divide_data_to_train_val_test(patient_numbers, shuffle=False):
    """

    :param shuffle
    :param patient_numbers:
    :return:
    """

    if shuffle:
        idx = np.arange(0, len(patient_numbers))
        np.random.shuffle(idx)
        patient_numbers = np.array(patient_numbers)[idx]

    train_set = patient_numbers[:int(0.8 * len(patient_numbers))]
    test_set = patient_numbers[int(0.8 * len(patient_numbers)):]

    validation_set = train_set[int(0.8 * len(train_set)):]
    train_set = train_set[:int(0.8 * len(train_set))]

    return train_set, validation_set, test_set


def create_dataset_for_patient(patient_number, binary_pred=True, postivie_class='N', aami_standarts=False):
    """

    :param patient_number:
    :param binary_pred:
    :param postivie_class: If it is a binary prediction - wich class is the 1.
    :param aami_standarts
    :return:
    """

    time, voltage1, voltage2, tags_time, tags, r_peaks_indexes = read_data_from_single_patient(patient_number)
    # internal_proccessed_signal = process_ecg_signal(voltage1, sampling_rate=360)
    r_peaks_indexes = r_peaks_indexes[1:]
    r_peaks_indexes = r_peaks_indexes[:(len(r_peaks_indexes) - 1)]

    beats = slice_beats_from_ecg_signal(voltage1, r_peaks_indexes, window_size=120, sample_rate=1)

    if binary_pred and not aami_standarts:
        tags = convert_tags(tags, postivie_class)
    elif binary_pred and aami_standarts:
        tags = convert_tags_according_to_aami(tags, postivie_class)
    elif not binary_pred and aami_standarts:
        tags = convert_tags_to_multiple_classes_aami(tags)
    else:
        tags = convert_tags_to_multiple_classes(tags)
    # Remove first and Last tags (This is true only for patient 100, need to be generic:)
    #beats = beats[1:]
    #beats = beats[:(len(beats) - 1)]
    tags = tags[1:]
    tags = tags[:(len(tags) - 1)]
    i = 0
    while i < len(beats):
        if len(beats[i]) == 0:
            del beats[i]
            del tags[i]
            continue
        i += 1

    # beats, tags = list(map(list, zip(*filter(lambda x: x[0] and x[1], zip(beats, tags)))))

    # print("Total number of heart beats found: %d" % len(beats))
    # print("Total number of tags: %d" % len(tags))
    assert len(tags) == len(beats)
    return np.array(beats), np.array(tags)


def convert_beats_for_lstm(x_ecg_beats, sequence_length):
    """

    :param patient_number:
    :param sequence_length:
    :param size_of_sample:
    :return:
    """

    x_data = np.array([x_ecg_beats[i: i + sequence_length]
                       for i in range(len(x_ecg_beats) - sequence_length)])

    # y_labels become [?, length_of_one_sample]
    y_labels = np.array([x_ecg_beats[i + sequence_length]
                         for i in range(len(x_ecg_beats) - sequence_length)])
    return x_data, y_labels


def slice_beats_from_ecg_signal(ecg_signal, r_peaks, window_size, sample_rate):
    """
    per beat in the signal we return the window [R-a, R+a], where R is the place where the peak occured. a is the window
    size. we will return a list of those windows. each window will be of size 2*a
    :param ecg_signal: a list of ecg samples
    :param r_peaks: indexes where there are peaks of the beats
    :param window_size
    :param sample_rate
    :return: list of windows of size 2 * window_size
    """
    # TODO: For testing I want also the times ...
    beats = [[] for x in r_peaks]  # Create empty lists as the size of number of beats
    print("Number of beats: %d" % len(r_peaks))
    for beat_number, peaks in enumerate(r_peaks):
        start = peaks - window_size
        # start = max(0, start)
        if start < 0:
            continue
        end = peaks + window_size
        # end = min(end, len(ecg_signal) - 1)
        if end > len(ecg_signal) - 1:
            continue
        for i in range(start, end, sample_rate):
            beats[beat_number].append(ecg_signal[i])

        # plt.plot(beats[beat_number])

    return beats


def process_ecg_signal(voltage_values=None, sampling_rate=360):
    """
    Uses an external lib biosspy to process the ecg signal
    :param voltage_values: array with all values of a singal ecg
    :param sampling_rate: how many samples for 1 second.
    :return:
    """
    res = ecg.ecg(voltage_values, sampling_rate, show=False)
    # plt.figure()
    # plt.plot(res[0][1500:4500:5], res[1][1500:4500:5])
    time_axis = res[0]
    filtered_ecg_signal = res[1]  # TODO: understand what it means filterd...
    r_peak_locations = res[2]  # each element here is the index of the r peak in the filtered_ecg_signal
    time_steps_for_heat_beat_templates = res[3]  # TODO: understand what is this...
    heart_beat_templates = res[4]
    heart_rate_time_series = res[5]
    heart_rates = res[6]
    return {"time_axis": time_axis,
            "filtered_signal": filtered_ecg_signal,
            "r_peaks": r_peak_locations,
            "time_steps_hb": time_steps_for_heat_beat_templates,
            "hb": heart_beat_templates,
            "time_steps_hr": heart_rate_time_series,
            "hr": heart_rates}


def read_gender_age_lead_from_patient(patient_number):
    """
    format of .hea file:

    100 2 360 650000
    100.dat 212 200 11 1024 995 -22131 0 MLII
    100.dat 212 200 11 1024 1011 20052 0 V5
    # 69 M 1085 1629 x1
    # Aldomet, Inderal

    :param patient_number:
    :return:
    """
    header_file = os.path.join(DATA_DIR, patient_number + '.hea')
    with open(header_file, 'r') as fd:
        for i, line in enumerate(fd):
            if i == 1:
                # Read first lead:
                line = line.split()
                first_lead = line[8]
            if i == 2:
                # Read second lead:
                line = line.split()
                second_lead = line[8]
            if i == 3:
                # Read second lead:
                line = line.split()
                age = int(line[1])
                gender = line[2]
            if i == 4:
                # TODO: add meds
                pass

        return first_lead, second_lead, age, gender


def read_data_from_single_patient(patient_number):
    """

    :param patient_number: string which represents the patient number.
    :return:
    """
    # patient_folder_path = os.path.join(DATA_DIR, patient_number)

    dat_file = os.path.join(DATA_DIR, patient_number + '.txt')
    # dat_file = "gs://ecg_bucket2/Data/text_files/" + patient_number + '.txt'
    time = []
    voltage1 = []
    voltage2 = []
    # with file_io.FileIO(dat_file, 'r') as fd:
    with open(dat_file, 'r') as fd:
        for line in fd:
            # print(line)
            line = line.split()
            time.append(line[0])
            voltage1.append(float(line[1]))
            voltage2.append(float(line[2]))

    tags_file = os.path.join(DATA_DIR, patient_number + '_tag.txt')
    # tags_file = "gs://ecg_bucket2/Data/text_files/" + patient_number + '_tag.txt'
    tags_time = []
    tags = []
    r_peaks_indexes = []
    # with file_io.FileIO(tags_file, 'r') as fd:
    with open(tags_file, 'r') as fd:
        for line in fd:
            line = line.split()
            tags_time.append(line[0])
            tags.append(line[2])
            r_peaks_indexes.append(int(line[1]))

    return time, voltage1, voltage2, tags_time, tags, r_peaks_indexes


def convert_tags_time(tags_time):
    """
    Converts the tags time as follows : 1:06.050 --> 66.6.050 (i.e from hours to minuets)
    :param tags_time:
    :return:
    """
    return [convert_hours_format_to_miuets(x) for x in tags_time ]


def convert_hours_format_to_miuets(hours_format):
    """

    :param hours_format:
    :return:
    """
    splited = hours_format.split(':')
    hours = splited[0]
    minuets = splited[1]
    ret = 60 * int(hours) + decimal.Decimal(minuets)
    return ret


def convert_tags(tags, postivie_class='N'):
    """

    :param tags:
    :param postivie_class: which class is considered as positive ie will be tagged with '1'.
    :return:
    """
    if postivie_class == 'O':
        # all beats that are not 'N', 'R', 'V', 'A':
        ret = [0 if x == 'N' or x == 'R' or x == 'V' or x == 'A' else 1 for x in tags]
    else:
        ret = [1 if x == postivie_class else 0 for x in tags]
    return ret


def convert_tags_according_to_aami(tags, positive_class='N'):
    """
    MAPPING THE MIT-BIH ARRHYTHMIA DATABASE HEARTBEAT TYPES TO THE AAMI HEARTBEAT CLASSES
    :param tags:
    :param positive_class:
    :return:
    """
    if positive_class == 'N':
        # All beats that are N or L or R or e or j:
        ret = [(1, x) if x == 'N' or x == 'L' or x == 'R' or x == 'e' or x == 'j' else (0, x) for x in tags]

    elif positive_class == 'S':
        # All beats that are : A or a or J or S:
        ret = [(1, x) if x == 'A' or x == 'a' or x == 'J' or x == 'S' else (0, x) for x in tags]
    elif positive_class == 'V':
        # All beats that are: V or E
        ret = [(1, x) if x == "V" or x == 'E' else (0, x) for x in tags]
    elif positive_class == 'F':
        ret = [(1, x) if x == "F" else (0, x) for x in tags]
    elif positive_class == 'Q':
        # Q or / or f
        ret = [(1, x) if x == "Q" or x == '/' or x == 'f' else (0, x) for x in tags]
    else:
        print("Unknown positive beat")
        raise ValueError("Unknown positive beat")
    return ret


def divide_data_to_train_test_according_to_aami():
    """

    :param shuffle
    :param patient_numbers:
    :return:
    """

    train_set = [101, 106, 108, 109, 112, 114, 115, 116, 118, 119, 122, 124, 201, 203, 205, 207, 208, 209, 215, 220,
                 223, 230]  # DS1
    train_set = [str(x) for x in train_set]
    test_set = [100, 103, 105, 111, 113, 117, 121, 123, 200, 202, 210, 212, 213, 214, 219, 221, 222, 228, 231, 232,
                233, 234]  # DS2
    test_set = [str(x) for x in test_set]

    validation_set = train_set[int(0.8 * len(train_set)):]
    train_set = train_set[:int(0.8 * len(train_set))]

    return train_set, validation_set, test_set


def convert_tags_to_multiple_classes_aami(tags):
    """
    convert to 5 different classes:
    0 - N
    1 - S
    2 - V
    3 - F
    4 - Q
    :param tags:
    :return: ret.
    """
    ret = []
    for tag in tags:
        x = tag
        if x == 'N' or x == 'L' or x == 'R' or x == 'e' or x == 'j':
            ret.append(0)
        elif x == 'A' or x == 'a' or x == 'J' or x == 'S':
            ret.append(1)
        elif x == "V" or x == 'E':
            ret.append(2)
        elif x == "F":
            ret.append(3)
        else:
            ret.append(4)
            if x != "Q" and x != '/' and x != 'f':
                print("Warning - uindentified beat")

    return ret


def convert_tags_to_multiple_classes(tags):
    """
    convert to 5 different classes:
    0 - other
    1 - normal beat
    2 - Atrial premature beat
    3 - !Premature ventricular contraction! Changed to R beat.
    4 - Aberrated atrial premature beat
    :param tags:
    :return: ret.
    """
    ret = []
    for tag in tags:
        if tag == "N":
            ret.append(1)
        elif tag == "A":
            ret.append(2)
        elif tag == "V":
            ret.append(3)
        elif tag == "R":
            ret.append(4)
        else:
            ret.append(0)

    return ret


def plot_ecg_signal(tags, tags_time, time, voltage, start_time, end_time, sampling_rate):
    """

    :param tags_time:
    :param time:
    :param voltage:
    :param start_time:
    :param end_time:
    :param sampling_rate:
    :return:
    """

    tags_time = convert_tags_time(tags_time)
    tags = convert_tags(tags)
    time_to_plot = time[start_time:end_time:sampling_rate]
    time_to_plot = [decimal.Decimal(x) for x in time_to_plot]
    voltage_to_plot = voltage[start_time:end_time:sampling_rate]

    # tags_time_to_plot = [[tags_time.index(x), x] for x in tags_time if x in time_to_plot]
    zipped = zip(tags_time, tags)
    tags_to_plot = [x for x in zipped if time_to_plot[0] <= x[0] <= time_to_plot[-1]]
    print(tags_to_plot)
    positives = [x for x in tags_to_plot if x[1] == 1]
    negatives = [x for x in tags_to_plot if x[1] == 0]

    tags_time_p, positive_tags = zip(*positives)
    tags_time_n, negative_tags = zip(*negatives)
    plt.plot(time_to_plot, voltage_to_plot)
    plt.plot(tags_time_p, positive_tags, 'ro')
    plt.plot(tags_time_n, negative_tags, 'ro', color='green')


def read_dat_files_of_patiens(data_dir):
    """

    :param data_dir:
    :return:
    """
    patients = {}
    for filename in os.listdir(data_dir):
        if filename.endswith(".dat"):
            patient_number = filename.split('.dat')[0]
            full_path = os.path.join(data_dir, filename)
            record = wfdb.rdsamp(full_path.split('.dat')[0])
            ecg_signal = record.p_signals
            '''
            voltage1 = []
            voltage2 = []
            for e in ecg_signal:
                with open(patient_number + ".txt", 'w') as f:
                    f.write(e)
                voltage1.append(e[0])
                voltage2.append(e[1])
            assert record.fs == 360
            patients[filename.split('.dat')[0]] = {}
            patients[filename.split('.dat')[0]]['v1'] = voltage1
            patients[filename.split('.dat')[0]]['v2'] = voltage2
            '''
            np.savetxt(patient_number + '.txt', ecg_signal)

        else:
            continue

    return patients


def get_list_of_all_patient_numbers(data_dir):
    """

    :param data_dir:
    :return: returns a list [100, 101, ...]  with all patient numbers
    """
    patients = []
    for filename in os.listdir(data_dir):
        if filename.endswith(".dat"):
            patient_number = filename.split('.dat')[0]
            patients.append(patient_number)

    print("Found total of %d patients" % len(patients))
    return patients


def get_statistics_of_beat_types(patient_numbers):
    """
    print how much beats of each type we have for each patient and total.
    :param patient_numbers:
    :return:
    """
    total_normal_beats = 0  # Normal beat
    total_A_beats = 0  # Atrial premature beat
    total_L_beats = 0  # Left bundle branch block beat
    total_R_beats = 0  # Right bundle branch block beat
    total_a_beats = 0  # Aberrated atrial premature beat
    total_J_beats = 0  # Nodal (junctional) premature beat
    total_S_beats = 0  # Supraventricular premature or ectopic beat (atrial or nodal)
    total_V_beats = 0  # Premature ventricular contraction
    total_F_beats = 0  # Fusion of ventricular and normal beat

    total_start_preths = 0  # "[" Start of ventricular flutter/fibrillation
    total_end_preths = 0  # "]" End of ventricular flutter/fibrillation
    total_vant_flut_wave = 0  # "!" Ventricular flutter wave

    total_e_beats = 0  # Atrial escape beat
    total_j_beats = 0  # Nodal (junctional) escape beat
    total_E_beats = 0  # Ventricular escape beat
    total_paced_beats = 0  # "/"
    total_f_beats = 0  # Fusion of paced and normal beat
    total_x_beats = 0  # Non-conducted P-wave (blocked APC)
    total_Q_beats = 0  # Unclassifiable beat
    total_isolated_beats = 0  # "|"

    for p in patient_numbers:
        _, _, _, _, tags, _ = read_data_from_single_patient(p)
        normal_beats = 0
        A_beats = 0
        L_beats = 0
        R_beats = 0
        a_beats = 0
        J_beats = 0
        S_beats = 0
        V_beats = 0
        F_beats = 0
        start_preths = 0  # "[" Start of ventricular flutter/fibrillation
        end_preths = 0  # "]" End of ventricular flutter/fibrillation
        vant_flut_wave = 0  # "!" Ventricular flutter wave
        e_beats = 0  # Atrial escape beat
        j_beats = 0  # Nodal (junctional) escape beat
        paced_beats = 0
        x_beats = 0
        isolated_beats = 0  # "|"
        E_beats = 0
        f_beats = 0
        Q_beats = 0

        ploos_beats = 0
        change_quality_beats = 0
        unrecognized_beat = 0

        for tag in tags:
            if tag == 'N':
                normal_beats += 1

            elif tag == 'A':
                A_beats += 1

            elif tag == 'L':
                L_beats += 1

            elif tag == 'R':
                R_beats += 1

            elif tag == 'a':
                a_beats += 1

            elif tag == 'J':
                J_beats += 1

            elif tag == 'S':
                S_beats += 1

            elif tag == 'V':
                V_beats += 1

            elif tag == 'F':
                F_beats += 1

            elif tag == '[':
                start_preths += 1

            elif tag == ']':
                end_preths += 1

            elif tag == '!':
                vant_flut_wave += 1

            elif tag == 'e':
                e_beats += 1

            elif tag == 'j':
                j_beats += 1

            elif tag == '/':
                paced_beats += 1

            elif tag == 'x':
                x_beats += 1

            elif tag == '|':
                isolated_beats += 1

            elif tag == 'E':
                E_beats += 1

            elif tag == 'f':
                f_beats += 1

            elif tag == 'Q':
                Q_beats += 1

            elif tag == '+':
                ploos_beats += 1

            elif tag == '~':
                change_quality_beats += 1

            elif tag == '"':
                unrecognized_beat += 1

            else:
                print("Unrecognized beat")
        total_normal_beats += normal_beats
        total_A_beats += A_beats
        total_L_beats += L_beats
        total_R_beats += R_beats
        total_a_beats += a_beats
        total_J_beats += J_beats
        total_S_beats += S_beats
        total_V_beats += V_beats
        total_F_beats += F_beats
        total_E_beats += E_beats
        total_f_beats += f_beats
        total_Q_beats += Q_beats
        total_start_preths += start_preths
        total_end_preths += end_preths
        total_vant_flut_wave += vant_flut_wave
        total_e_beats += e_beats
        total_j_beats += j_beats
        total_paced_beats += paced_beats
        total_x_beats += x_beats
        total_isolated_beats += isolated_beats

        '''
        print("For patient %s:" % p)
        print("Number of Normal beats:", normal_beats)
        print("Number of A beats:", A_beats)
        print("Number of L beats:", L_beats)
        print("Number of R beats:", R_beats)
        print("Number of a beats:", a_beats)
        print("Number of J beats:", J_beats)
        print("Number of S beats:", S_beats)
        print("Number of V beats:", V_beats)
        print("Number of F beats:", F_beats)
        print("Number of E beats:", E_beats)
        print("Number of f beats:", f_beats)
        print("Number of Q beats:", Q_beats)
        print("Number of start_preths beats:", start_preths)
        print("Number of end_preths beats:", end_preths)
        print("Number of vant_flut_wave beats:", vant_flut_wave)
        print("Number of e_beats beats:", e_beats)
        print("Number of j beats:", j_beats)
        print("Number of paced beats:", paced_beats)
        print("Number of x beats:", x_beats)
        print("Number of isolated beats:", isolated_beats)
        '''
    # print("Total Beats Statistics")
    print("Number of Normal beats:", total_normal_beats)
    print("Number of A beats:", total_A_beats)
    print("Number of L beats:", total_L_beats)
    print("Number of R beats:", total_R_beats)
    print("Number of a beats:", total_a_beats)
    print("Number of J beats:", total_J_beats)
    print("Number of S beats:", total_S_beats)
    print("Number of V beats:", total_V_beats)
    print("Number of F beats:", total_F_beats)
    print("Number of E beats:", total_E_beats)
    print("Number of f beats:", total_f_beats)
    print("Number of Q beats:", total_Q_beats)
    print("Number of total_start_preths beats:", total_start_preths)
    print("Number of end prethes beats:", total_end_preths)
    print("Number of vant_flut beats:", total_vant_flut_wave)
    print("Number of e beats:", total_e_beats)
    print("Number of j beats:", total_j_beats)
    print("Number of paced beats:", total_paced_beats)
    print("Number of x beats:", total_x_beats)
    print("Number of isolated beats:", total_isolated_beats)


def plot_beat_number(beat_number, patient_number):
    """
    plot beat number beat_number of patient patient_number according to the text file
    :param beat_number:
    :param patient_number:
    :param window_size:
    :param samplinng_rate:
    :return:
    """
    beats, tags = create_dataset_for_patient(patient_number, binary_pred=False)

    beat_to_print = beats[beat_number]
    type_of_beat = tags[beat_number]

    plt.figure()
    plt.plot(beats[beat_number - 1])

    plt.figure()
    plt.plot(beat_to_print)

    plt.figure()
    plt.plot(beats[beat_number + 1])
    plt.show()
    print("Type of Beat: %d" % int(type_of_beat))


def get_all_beats_of_type(generated_class, train_beats, train_tags):
    """

    :param generated_class:
    :param train_beats:
    :param train_tags:
    :return:
    """
    filterd_beats = [train_beats[i] for i, e in enumerate(train_tags) if e == generated_class]
    filterd_tags = [x for x in train_tags if x == generated_class]
    return np.array(filterd_beats), np.array(filterd_tags)


def get_all_beats_of_type_aami(class_number_to_generate, train_beats, train_tags):
    """

    :param class_number_to_generate:
    :param train_beats:
    :param train_tags:
    :return:
    """

if __name__ == "__main__":

    # time, voltage1, voltage2, tags_time, tags, r_peaks_indexes = read_data_from_single_patient('100')

    # plot_ecg_signal(tags, tags_time, time, voltage1, 1500, 4500, 5)
    '''    
    r = process_ecg_signal(voltage1)
    
    plt.show()
    '''
    # beats, tags = create_dataset_for_patient('100')
    # data_dir = os.path.join('Data', 'MIT database')
    # patients = read_dat_files_of_patiens(data_dir)
    # print(patients['100'])
    # print(get_list_of_all_patient_numbers(data_dir))
    # get_statistics_of_beat_types(get_list_of_all_patient_numbers(data_dir))
    # print("DONE")
    # beats, tags = create_dataset_for_patient('100', binary_pred=False)

    # x_data, y_labels = convert_beats_for_lstm(beats, sequence_length=3)
    # plot_beat_number(patient_number='100', beat_number=440)
    '''
    pt_numbers = get_list_of_all_patient_numbers(os.path.join('Data', 'MIT database'))
    for p in pt_numbers:
        print(read_gender_age_lead_from_patient(p))
    '''
    create_beat_samples_aami(positive_class=None)
