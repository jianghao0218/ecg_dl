import tensorflow as tf

class EcgParam:
    def __init__(self):
        self.N = 256
        self.hrstd = 1.0
        self.hrmean = 60.0
        self.lfhfratio = 0.5
        self.sfecg = 256
        self.sf = 512
        self.amplitude = 1.4
        self.seed = 1
        self.anoise = 0.1
        self.period = None

        # Define frequency parameters for rr process:
        # flo and fhi correspond to the Mayer waves and respiratory rate respectively:
        self.flo = 0.1
        self.fhi = 0.25
        self.flostd = 0.01
        self.fhistd = 0.01

        # Order of extrema: [P Q R S T]:
        '''
        self.theta = [0 for e in range(5)]
        self.a = [0 for e in range(5)]
        self.b = [0 for e in range(5)]
        
        self.theta[0] = -60.0
        self.theta[1] = -15.0
        self.theta[2] = 0.0
        self.theta[3] = 15.0
        self.theta[4] = 90.0

        self.a[0] = 1.2
        self.a[1] = -5.0
        self.a[2] = 30.0
        self.a[3] = -7.5
        self.a[4] = 0.75

        self.b[0] = 0.25
        self.b[1] = 0.1
        self.b[2] = 0.1
        self.b[3] = 0.1
        self.b[4] = 0.4
        '''
        a = [1.2, -5.0, 30.0, -7.5, 0.75]
        self.a = tf.get_variable("a", initializer=tf.constant_initializer(a))
        b = [0.25, 0.1, 0.1, 0.1, 0.4]
        self.b = tf.get_variable("b", initializer=tf.constant_initializer(b))
        theta = [-60.0, -15.0, 0.0, 15.0, 90.0]
        self.theta = tf.get_variable("theta", initializer=tf.constant_initializer(theta))

        self.ecgAnimateInterval = (1000 / (self.sfecg))

        self.allParametersValid = True

    # Set/Get Parameter Functions:
    def setN(self, value):
        """

        :param value:
        :return:
        """
        self.N = value
        self.allParametersValid = False

    def getN(self):
        return self.N

    def setHrStd(self, value):
        self.hrstd = value
        self.allParametersValid = False

    def getHrStd(self):
        return self.hrstd

    def setHrMean(self, value):
        self.hrmean = value
        self.allParametersValid = False

    def getHrMean(self):
        return self.hrmean

    def setLfHfRatio(self, value):
        self.lfhfratio = value
        self.allParametersValid = False

    def getLfHfRatio(self):
        return self.lfhfratio

    def setSfEcg(self, value):
        self.sfecg = value
        self.allParametersValid = False

    def getSfEcg(self):
        return self.sfecg

    def setSf(self, value):
        self.sf = value
        self.allParametersValid = False

    def getSf(self):
        return self.sf

    def setAmplitude(self, value):
        self.amplitude = value
        self.allParametersValid = False

    def getAmplitude(self):
        return self.amplitude

    def setSeed(self, value):
        self.seed = value
        self.allParametersValid = False

    def getSeed(self):
        return self.seed

    def setANoise(self, value):
        self.anoise = value
        self.allParametersValid = False

    def getANoise(self):
        return self.anoise

    def setPeriod(self, value):
        self.period = value
        self.allParametersValid = False

    def getPeriod(self):
        return self.period

    def setFLo(self, value):
        self.flo = value
        self.allParametersValid = False

    def getFLo(self):
        return self.flo

    def setFHi(self, value):
        self.fhi = value
        self.allParametersValid = False

    def getFHi(self):
        return self.fhi

    def setFLoStd(self, value):
        self.flostd = value
        self.allParametersValid = False

    def getFLoStd(self):
        return self.flostd

    def setFHiStd(self, value):
        self.fhistd = value
        self.allParametersValid = False

    def getFHiStd(self):
        return self.fhistd

    def setTheta(self, index, value):
        self.theta[index] = value
        self.allParametersValid = False

    def getTheta(self, index):
        return self.theta[index]

    def setA(self, index, value):
        self.a[index] = value
        self.allParametersValid = False

    def getA(self, index):
        return self.a[index]

    def setB(self, index, value):
        self.b[index] = value
        self.allParametersValid = False

    def getB(self, index):
        return self.b[index]

    def setEcgAnimateInterval(self, value):
        self.ecgAnimateInterval = value
        self.allParametersValid = False

    def getEcgAnimateInterval(self):
        return self.ecgAnimateInterval

    # Check to see if all parameters are valid:
    def isValid(self):
        return self.allParametersValid

    def checkParameters(self):

        RetValue = True
        self.allParametersValid = True

        # Check the Internal frequency respect to ECG frequency
        if  int(self.sf % self.sfecg) != 0:
                RetValue = False
                self.allParametersValid = False

        return RetValue



