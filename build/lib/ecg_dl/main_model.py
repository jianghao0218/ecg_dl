from ecg_dl import LSTM_classifier
from ecg_dl import multi_class_classifier
import argparse
import os
import pickle
import numpy as np
import tensorflow as tf


def load_dataset_from_pickle(pickle_file_path):
    """

    :param pickle_file_path:
    :return:
    """
    pickle_in = open(pickle_file_path, "rb")
    print("Loading dataset from pickle file %s ..." % pickle_file_path)
    loaded_dataset = pickle.load(pickle_in)
    print("**Loaded done**")
    beats_train = loaded_dataset['Train_beats']
    train_tags = loaded_dataset['Train_tags']
    beats_val = loaded_dataset['Val_beats']
    validation_tags = loaded_dataset['Val_tags']
    beats_test = loaded_dataset['Test_beats']
    test_tags = loaded_dataset['Test_tags']
    return beats_train, train_tags, beats_val, validation_tags, beats_test, test_tags


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--classifier_architecture', type=str, help='Which type of network to use',
                        choices=['LSTM', 'FC', 'TRAIN_GAN'])
    parser.add_argument('--network_architecture_file', type=str, help='path to file with the architecture of the '
                                                                      'network')
    parser.add_argument('--database_type', type=str, help='which way to divide the data', choices=['AAMI', 'REGULAR'],
                        default='REGULAR')
    parser.add_argument('--tensor_board_dir', type=str, default='./logs', help='directory to save log events')
    parser.add_argument('--saved_models_dir', type=str, default='./saved_models', help='directory to save model values')
    parser.add_argument('--beat_to_classify', type=str, default='ALL',
                        help='which beat to classify against all others. '
                             'if All is chosen then a multi class classifier will be trained', choices=
                        ['A', 'R', 'V', 'N', 'O', 'ALL', 'S', 'Q', 'F'])

    parser.add_argument('--use_gan', action='store_true', help='Use a generator and add synthetic data')
    parser.add_argument('--gan_type', type=str, default='A', help='Type of ECG class to generate',
                        choices=['A', 'R', 'V', 'N', 'O', 'Q', 'S', 'F', 'DC_CONDITIONAL'])
    parser.add_argument('--gan_meta_file', type=str, help='The meta file of the GAN')
    parser.add_argument('--num_of_examples_to_add_from_gan', default=0, type=int,
                        help='How many examples to generate from the GAN')

    parser.add_argument('--add_noise', action='store_true', help='add noise as beats')
    parser.add_argument('--mode', type=str, help='Which mode to apply the LSTM',
                        choices=['TRAIN', 'TEST', 'TRAIN_V', 'TRAIN_R', 'TRAIN_A', 'TRAIN_N', 'TRAIN_O', 'TRAIN_S',
                                 'TRAIN_Q', 'TRAIN_F'],
                        default='TRAIN')
    parser.add_argument('--best_acc_validation_value', type=float, help='what was the best accuracy on the validation '
                                                                        'set when training')
    parser.add_argument('--loaded_graph_name', type=str, help='Name of the meta file of the trained LSTM')
    parser.add_argument('--meta_file_directory', type=str, help="Path to the directory of the meta file")

    parser.add_argument('--save_data_to_pickle', action='store_true', help='Save dataset into a pickle file')
    parser.add_argument('--pickle_path', type=str, help='pickle path to save or load the dataset', default=None)
    parser.add_argument('--use_data_from_pickle', action='store_true', help='Load dataset from oickle file')

    parser.add_argument('--path_to_save_roc_curve', help='Path to where to store the roc curve png file', type=str)
    parser.add_argument('--path_to_save_prec_rec', help='Path to where to store the prec rec png file', type=str)


    args = parser.parse_args()

    print("*****Running ECG classifier******")
    print("Model parameters:")
    print("Network type:", args.classifier_architecture)
    print("Database type: ", args.database_type)
    print("Save model dir:", args.saved_models_dir)
    print("Logs dir:", args.tensor_board_dir)
    print("Use Gan ? : ", args.use_gan)
    print("Number of examples to use from GAN:", args.num_of_examples_to_add_from_gan)
    print("The roc curve file will be saved at:", args.path_to_save_roc_curve)

    if args.classifier_architecture == 'TRAIN_GAN':
        pass
    else:
        if args.use_gan:
            print("GAN Type", args.gan_type)

        if args.use_data_from_pickle:
            beats_train, train_tags, beats_val, validation_tags, _beats_test, test_tags = \
                load_dataset_from_pickle(args.pickle_path)
        else:
            if args.database_type == 'REGULAR':
                if args.beat_to_classify == 'ALL':
                    if args.classifier_architecture == 'LSTM':
                        beats_train, train_tags, beats_val, validation_tags, beats_test, test_tags = \
                            LSTM_classifier.create_dataset( os.path.join('Data', 'MIT database'), number_of_classes=5,
                                                            length_of_each_sample=5,
                                                            save_to_pickle=args.save_data_to_pickle,
                                                            pickle_path=args.pickle_path, positive_class=None)
                    else:
                        assert args.classifier_architecture == 'FC'
                        beats_train, train_tags, beats_val, validation_tags, _beats_test, test_tags = \
                            multi_class_classifier.create_dataset(os.path.join('Data', 'MIT database'), 5,
                                                                  save_to_pickle=args.save_data_to_pickle,
                                                                  pickle_path=args.pickle_path, positive_class=None)

                else:
                    if args.classifier_architecture == 'LSTM':

                        beats_train, train_tags, beats_val, validation_tags, beats_test, test_tags = LSTM_classifier.\
                            create_dataset( os.path.join('Data', 'MIT database'), number_of_classes=2,
                                            length_of_each_sample=5, save_to_pickle=args.save_data_to_pickle,
                                            pickle_path=args.pickle_path, positive_class=args.beat_to_classify)
                    else:
                        assert args.classifier_architecture == 'FC'
                        beats_train, train_tags, beats_val, validation_tags, beats_test, test_tags = \
                            multi_class_classifier.create_dataset(os.path.join('Data', 'MIT database'),
                                                                   number_of_classes=2,
                                                                   save_to_pickle=args.save_data_to_pickle,
                                                                   pickle_path=args.pickle_path,
                                                                   positive_class=args.beat_to_classify)

            else:
                assert args.database_type == 'AAMI'
                if args.classifier_architecture == 'LSTM':
                    beats_train, train_tags, beats_val, validation_tags, beats_test, test_tags = \
                        LSTM_classifier.create_dataset_according_to_aami(positive_class=args.beat_to_classify,
                                                                         length_of_each_sample=5,
                                                                         save_to_pickle=args.save_data_to_pickle,
                                                                         pickle_path=args.pickle_path,
                                                                         print_statistics=False,
                                                                         drop_other_beats=True)

                else:
                    assert args.classifier_architecture == 'FC'
                    beats_train, train_tags, beats_val, validation_tags, beats_test, test_tags = \
                        multi_class_classifier.create_dataset_according_to_aami(positive_class=args.beat_to_classify,
                                                                                length_of_each_sample=5,
                                                                                save_to_pickle=args.save_data_to_pickle,
                                                                                pickle_path=args.pickle_path,
                                                                                print_statistics=False,
                                                                                drop_other_beats=True)

        if args.use_gan:
            print("Adding synthetic %s beats:" % args.gan_type)
            if args.mode == 'TRAIN':
                number_of_classes = 5
            else:
                number_of_classes = 2
            if args.classifier_architecture == 'LSTM':
                if args.add_noise:
                    print("Adding Noise")
                    gan_beats, gan_tags = LSTM_classifier.add_random_noise(number_samples_to_generate=
                                                                           args.num_of_examples_to_add_from_gan,
                                                                           length_of_each_sample=5)
                else:
                    if args.gan_type != "DC_CONDITIONAL":
                        gan_beats, gan_tags = LSTM_classifier.add_data_from_gan(args.gan_meta_file,
                                                                                number_samples_to_generate=
                                                                                args.num_of_examples_to_add_from_gan,
                                                                                type_of_beat_generated=args.gan_type,
                                                                                length_of_each_sample=5,
                                                                                number_of_classes=number_of_classes)
                    else:
                        if args.beat_to_classify == 'N':
                            y_index = 0
                        elif args.beat_to_classify == 'S':
                            y_index = 1
                        elif args.beat_to_classify == 'V':
                            y_index = 2
                        elif args.beat_to_classify == 'F':
                            y_index = 3
                        else:
                            assert args.beat_to_classify == 'Q'
                            y_index = 4

                        print("Adding Data from DC-Conditional GAN with label: ", args.beat_to_classify)
                        gan_beats, gan_tags = LSTM_classifier.add_data_from_gan(args.gan_meta_file,
                                                                                number_samples_to_generate=
                                                                                args.num_of_examples_to_add_from_gan,
                                                                                type_of_beat_generated=args.gan_type,
                                                                                length_of_each_sample=5,
                                                                                number_of_classes=number_of_classes,
                                                                                is_conditional_gan=True,
                                                                                label_index=y_index)

            else:
                assert args.classifier_architecture == 'FC'
                gan_beats, gan_tags = multi_class_classifier.add_data_from_gan(args.gan_meta_file,
                                                                               number_samples_to_generate=
                                                                               args.num_of_examples_to_add_from_gan,
                                                                               type_of_beat_generated=args.gan_type,
                                                                               meta_file_directory=
                                                                               args.gan_meta_file_directory,
                                                                               number_of_classes=number_of_classes)

            beats_train = np.concatenate((beats_train, gan_beats), axis=0)
            train_tags = np.concatenate((train_tags, gan_tags), axis=0)

            # Don't need the gan graph now, so clean it:
            tf.reset_default_graph()

        if args.mode == 'TRAIN' or args.mode == 'TEST':
            if args.classifier_architecture == 'LSTM':
                ecg_classifier = LSTM_classifier.create_lstm_classifier(number_of_classes=5, sequence_length=48,
                                                                        length_of_each_sample=5,
                                                                  tensor_board_dir=args.tensor_board_dir,
                                                                  save_model_dir=args.saved_models_dir,
                                                                  dropout_prob=1, mode=args.mode)

            else:
                assert args.classifier_architecture == 'FC'
                ecg_classifier = multi_class_classifier.classifier(network_architecture_file=
                                                                   args.network_architecture_file,
                                                                   tensor_board_dir=args.tensor_board_dir,
                                                                   save_model_dir=args.saved_models_dir,
                                                                   dropout_prob=1)

        else:
            if args.classifier_architecture == 'LSTM':
                ecg_classifier = LSTM_classifier.create_lstm_classifier(number_of_classes=2, sequence_length=48,
                                                                        length_of_each_sample=5,
                                                                        tensor_board_dir=args.tensor_board_dir,
                                                                        save_model_dir=args.saved_models_dir,
                                                                        dropout_prob=1, mode=args.mode)
            else:
                assert args.classifier_architecture == 'FC'
                ecg_classifier = multi_class_classifier.classifier(network_architecture_file=
                                                                   args.network_architecture_file,
                                                                   tensor_board_dir=args.tensor_board_dir,
                                                                   save_model_dir=args.saved_models_dir,
                                                                   dropout_prob=1)

        if args.mode in ['TRAIN', 'TRAIN_V', 'TRAIN_R', 'TRAIN_A', 'TRAIN_N', 'TRAIN_O', 'TRAIN_Q', 'TRAIN_F',
                         'TRAIN_S']:

            if args.classifier_architecture == 'LSTM':
                LSTM_classifier.train(ecg_classifier, train_beats=beats_train, train_tags=train_tags,
                                      validation_beats=beats_val, validation_tags=validation_tags,
                                      number_of_iterations=4000, batch_size=16)

                LSTM_classifier.evaluate_classifier(ecg_classifier=ecg_classifier, test_beats=beats_test,
                                                    test_tags=test_tags, positive_class=args.mode)

                if args.mode in ['TRAIN_V', 'TRAIN_R', 'TRAIN_A', 'TRAIN_N', 'TRAIN_O', 'TRAIN_Q', 'TRAIN_F', 'TRAIN_S']:
                    LSTM_classifier.calc_precision_recall(ecg_classifier, beats_test, test_tags, meta_file_name=None,
                                                          save_path=os.path.join(args.path_to_save_prec_rec))

                    LSTM_classifier.celc_roc_curve(ecg_classifier, beats_test, test_tags,
                                                   save_path=os.path.join(args.path_to_save_roc_curve))

            else:
                assert args.classifier_architecture == 'FC'
                multi_class_classifier.train(ecg_classifier, beats_train, train_tags, beats_val, validation_tags,
                                             number_of_iterations=4000, batch_size=120)
                multi_class_classifier.evaluate_classifier(ecg_classifier, beats_test, test_tags,
                                                           positive_class=args.mode)

                multi_class_classifier.calc_precision_recall(ecg_classifier, beats_test, test_tags, meta_file_name=None,
                                                             meta_file_directory=None)
                multi_class_classifier.calc_roc_curve(ecg_classifier, beats_test, test_tags,
                                                      save_path=os.path.join(args.path_to_save_roc_curve))

        '''
        elif args.mode == "TEST":
    
            evaluate_classifier_with_best_validation(ecg_lstm, lstm_beats_test, test_tags, args.loaded_graph_name,
                                                     meta_file_directory=args.meta_file_directory,
                                                     val_beats=lstm_beats_val, val_tags=validation_tags,
                                                     best_val_acc_value=args.best_acc_validation_value,
                                                     positive_class='TRAIN_V')
    
            calc_precision_recall(ecg_lstm, lstm_beats_test, test_tags, meta_file_name=args.loaded_graph_name,
                                  meta_file_directory=args.meta_file_directory)
    
            celc_roc_curve(ecg_lstm, lstm_beats_test, test_tags, meta_file_name=args.loaded_graph_name,
                           meta_file_directory=args.meta_file_directory, save_path=
                           os.path.join('results', args.beat_to_classify, 'roc_curve_' +
                                        str(args.num_of_examples_to_add_from_gan)) + '_early_stop_auto_script.png')
        '''
